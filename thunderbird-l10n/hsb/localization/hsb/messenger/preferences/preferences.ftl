# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pane-general-title = Powšitkowne
category-general =
    .tooltiptext = { pane-general-title }
general-language-and-appearance-header = Rěč a zwonkowne
general-incoming-mail-header = Dochadźace mejlki
general-files-and-attachment-header = Dataje a přiwěški
general-tags-header = Znački
general-reading-and-display-header = Čitanje a zwobraznjenje
general-updates-header = Aktualizacije
general-network-and-diskspace-header = Syć a tačelowy rum
general-indexing-label = Indeksowanje
composition-category-header = Pisać
composition-attachments-header = Přiwěški
composition-spelling-title = Prawopis
compose-html-style-title = HTML-stil
composition-addressing-header = Adresować
privacy-main-header = Priwatnosć
privacy-passwords-header = Hesła
privacy-junk-header = Čapor
privacy-data-collection-header = Zběranje a wužiwanje datow
privacy-security-header = Wěstota
privacy-scam-detection-title = Wotkrywanje wobšudstwa
privacy-anti-virus-title = Antiwirusowy program
privacy-certificates-title = Certifikaty
chat-pane-header = Chat
chat-status-title = Status
chat-notifications-title = Zdźělenja
chat-pane-styling-header = Formatowanje
choose-messenger-language-description = Wubjerće rěče, kotrež so wužiwaja, zo bychu menije, powěsće a zdźělenki z { -brand-short-name } pokazali.
manage-messenger-languages-button =
    .label = Alternatiwy definować…
    .accesskey = l
confirm-messenger-language-change-description = Startujće { -brand-short-name } znowa. zo byšće tute změny nałožił
confirm-messenger-language-change-button = Nałožić a znowa startować
update-pref-write-failure-title = Pisanski zmylk
# Variables:
#   $path (String) - Path to the configuration file
update-pref-write-failure-message = Nastajenje njeda so składować. Njebě móžno do dataje pisać: { $path }
update-setting-write-failure-title = Zmylk při składowanju aktualizowanskich nastajenjow
# Variables:
#   $path (String) - Path to the configuration file
# The newlines between the main text and the line containing the path is
# intentional so the path is easier to identify.
update-setting-write-failure-message =
    { -brand-short-name } je na zmylk storčił a njeje tutu změnu składował. Dźiwajće na to, zo sej tute aktualizowanske nastajenje pisanske prawo za slědowacu dataju wužaduje. Wy abo systemowy administrator móžetej zmylk porjedźić, hdyž wužiwarskej skupinje połnu kontrolu nad tutej dataju datej.
    
    Njeda so do dataje pisać: { $path }
update-in-progress-title = Aktualizacija běži
update-in-progress-message = Chceće, zo { -brand-short-name } z tutej aktualizaciju pokročuje?
update-in-progress-ok-button = &Zaćisnyć
# Continue is the cancel button so pressing escape or using a platform standard
# method of closing the UI will not discard the update.
update-in-progress-cancel-button = &Dale
