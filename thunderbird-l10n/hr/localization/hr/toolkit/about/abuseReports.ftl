# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Prijavite ovaj dodatak prema { -vendor-short-name }
abuse-report-title-theme = Prijavite ovu temu prema { -vendor-short-name }
abuse-report-subtitle = Što je problem?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = od <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-learnmore =
    Niste sigurni koji problem trebate odabrati?
    <a data-l10n-name="learnmore-link">Saznajte više o prijavljivanju problema s dodacima i temama</a>
abuse-report-submit-description = Opišite problem (opcionalno)
abuse-report-textarea =
    .placeholder = Lakše nam je ispraviti pogrešku ukoliko znamo detalje problema. Molimo vas da opištete događaj. Hvala vam što nam pomažete održati web zdravim.
abuse-report-submit-note =
    Napomena: Nemojte unositi osobne podatke (kao što su ime, adresa e-pošte, broj telefona, adresa).
    { -vendor-short-name } trajno čuva zapise ovih izvještaja.

## Panel buttons.

abuse-report-cancel-button = Odustani
abuse-report-next-button = Dalje
abuse-report-goback-button = Idi natrag
abuse-report-submit-button = Pošalji

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on

abuse-report-messagebar-aborted = Prijava za <span data-l10n-name="addon-name">{ $addon-name }</span> je otkazana.
abuse-report-messagebar-submitting = Slanje prijave za <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-submitted = Hvala vam što ste poslali prijavu. Želite li ukloniti <span data-l10n-name="addon-name">{ $addon-name }</span>?
abuse-report-messagebar-submitted-noremove = Hvala vam što ste poslali prijavu.
abuse-report-messagebar-removed-extension = Hvala vam što ste poslali prijavu. Uklonili ste dodatak <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-removed-theme = Hvala vam što ste poslali prijavu. Uklonili ste temu <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error = Došlo je do greške prilikom slanja prijave za <span data-l10n-name="addon-name">{ $addon-name }</span>.
abuse-report-messagebar-error-recent-submit = Prijava za <span data-l10n-name="addon-name">{ $addon-name }</span> nije poslana iz razloga što je druga prijava nedavno poslana.

## Message bars actions.

abuse-report-messagebar-action-remove-extension = Da, ukloni
abuse-report-messagebar-action-keep-extension = Ne, zadržat ću
abuse-report-messagebar-action-remove-theme = Da, ukloni
abuse-report-messagebar-action-keep-theme = Ne, zadržat ću
abuse-report-messagebar-action-retry = Pokušaj ponovno
abuse-report-messagebar-action-cancel = Odustani

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-damage-reason = Oštećuje moje računalo i podatke
abuse-report-damage-example = Primjer: Ubrizgani zlonamjerni program ili ukradeni podaci
abuse-report-spam-reason = Stvara neželjenu poštu (spam) ili reklame
abuse-report-spam-example = Primjer: Umeće reklame na web stranice
abuse-report-settings-reason = Promijenio je moj pretraživač, početnu stranicu ili novu karticu bez da me obavijestio ili pitao za dopuštenje
abuse-report-settings-suggestions = Prije prijave dodatka, možete pokušati izmijeniti svoje postavke:
abuse-report-settings-suggestions-search = Izmijenite svoje zadane postavke pretraživanja
abuse-report-settings-suggestions-homepage = Izmijenite svoju početnu stranicu ili novu karticu
abuse-report-deceptive-reason = Pretvara se da je nešto što nije
abuse-report-deceptive-example = Primjer: Obmanjujući opis ili slike
abuse-report-broken-reason-extension = Ne radi, kvari web stranice ili usporava { -brand-product-name }
abuse-report-broken-reason-theme = Ne radi ili kvari prikaz preglednika
abuse-report-broken-example = Primjer: Značajke su spore, teško ih je koristiti ili ne rade, dijelovi web stranica se ne žele učitati ili izgledaju kako ne bi trebali
abuse-report-broken-suggestions-extension =
    Zvuči kao da ste pronašli pogrešku u programu. Uz slanje prijave ovdje, najbolji način
    za rješavanje problema s funkcionalnosti je da kontaktirate razvijatelja dodatka.
    <a data-l10n-name="support-link">Posjetite web stranicu dodatka</a> kako biste pronašli informacije o razvijatelju.
abuse-report-broken-suggestions-theme =
    Zvuči kao da ste pronašli pogrešku u programu. Uz slanje prijave ovdje, najbolji način
    za rješavanje problema s funkcionalnosti je da kontaktirate razvijatelja teme.
    <a data-l10n-name="support-link">Posjetite web stranicu teme</a> kako biste pronašli informacije o razvijatelju.
abuse-report-policy-reason = Sadržaj mržnje, nasilja ili ilegalni
abuse-report-policy-suggestions =
    Napomena: Problemi s autorskim pravima i zaštitnim znakovima moraju se prijaviti u
    odvojenom procesu. <a data-l10n-name="report-infringement-link">Koristite ova upute</a> za prijavu problema.
abuse-report-unwanted-reason = Nikad nisam želio ovaj dodatak i ne mogu ga ukloniti
abuse-report-unwanted-example = Primjer: Aplikacija instalirana bez mog dopuštenja
abuse-report-other-reason = Nešto drugo
