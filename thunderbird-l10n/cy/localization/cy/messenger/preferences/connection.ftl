# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Defnyddio Darparwr
    .accesskey = D
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (Rhagosodiad)
    .tooltiptext = Defnyddiwch yr URL rhagosodedig ar gyfer datrys DNS dros HTTPS
connection-dns-over-https-url-custom =
    .label = Cyfaddas
    .accesskey = C
    .tooltiptext = Rhowch eich hoff URL er mwyn datrys DNS drod HTTPS
connection-dns-over-https-custom-label = Cyfaddas
