<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label                   "ძირითადი">
<!ENTITY itemAutoComplete.label              "მისამართები">
<!ENTITY itemSpellCheck.label                "მართლწერა">

<!ENTITY dialogCompose.title                 "შედგენა">

<!ENTITY forwardMsg.label                     "წერილების გადაგზავნა:">
<!ENTITY forwardMsg.accesskey                 "დ">
<!ENTITY inline.label                         "წერილშივე">
<!ENTITY asAttachment.label                   "დანართად">
<!ENTITY addExtension.label                   "გაფართოების დამატება ფაილის სახელისთვის">
<!ENTITY addExtension.accesskey               "გ">

<!ENTITY htmlComposeHeader.label              "HTML">
<!ENTITY font.label                           "შრიფტი:">
<!ENTITY font.accesskey                       "შ">
<!ENTITY fontSize.label                       "ზომა:">
<!ENTITY fontSize.accesskey                   "ზ">
<!ENTITY useReaderDefaults.label              "წამკითხველის ნაგულისხმევი ფერების გამოყენება">
<!ENTITY useReaderDefaults.accesskey          "წ">
<!ENTITY fontColor.label                      "ტექსტის ფერი:">
<!ENTITY fontColor.accesskey                  "ტ">
<!ENTITY bgColor.label                        "ფონის ფერი:">
<!ENTITY bgColor.accesskey                    "ფ">
<!ENTITY restoreHTMLDefaults.label            "ნაგულისხმების აღდგენა">
<!ENTITY restoreHTMLDefaults.accesskey        "ნ">
<!ENTITY defaultToParagraph.label             "შიგთავსში ნაგულისხმევად, აბზაცების გამოყენება, ჩვეულებრივი ტექსტის ნაცვლად">
<!ENTITY defaultToParagraph.accesskey         "ბ">

<!ENTITY spellCheck.label                     "მართლწერის შემოწმება გაგზავნამდე">
<!ENTITY spellCheck.accesskey                 "მ">
<!ENTITY spellCheckInline.label               "მართლწერის შემოწმება ტექსტის შეტანისას">
<!ENTITY spellCheckInline1.accesskey          "ლ">
<!ENTITY languagePopup.label                  "ენა:">
<!ENTITY languagePopup.accessKey              "ე">
<!ENTITY downloadDictionaries.label           "სხვა ლექსიკონების ჩამოტვირთვა">

<!ENTITY warnOnSendAccelKey.label             "დასტური წერილის გაგზავნისთვის მალმხმობის გამოყენებისას">
<!ENTITY warnOnSendAccelKey.accesskey         "დ">
<!ENTITY autoSave.label                       "თვითშენახვა ყოველ">
<!ENTITY autoSave.accesskey                   "თ">
<!ENTITY autoSaveEnd.label                    "წუთში">

<!ENTITY emailCollectionPicker.label           "გამავალი ელფოსტის მისამართების ავტომატური ჩამატება ჩემს:">
<!ENTITY emailCollectionPicker.accesskey       "გ">
<!ENTITY addressingTitle.label                 "მისამართების თვითდასრულება">
<!ENTITY autocompleteText.label                "წერილების გაგზავნისას შესაბამისი ჩანაწერების შემოწმება:">
<!ENTITY addressingEnable.label                "ადგილობრივ წიგნაკებში">
<!ENTITY addressingEnable.accesskey            "დ">
<!ENTITY directories.label                     "საქაღალდეების სერვერი:">
<!ENTITY directories.accesskey                 "რ">
<!ENTITY directoriesNone.label                 "არა">
<!ENTITY editDirectories.label                 "საქაღალდეების ჩასწორება…">
<!ENTITY editDirectories.accesskey             "ჩ">
<!ENTITY showAsDefault.label                   "ნაგულისხმევი გამშვები საქაღალდე, მისამართების წიგნაკის ფანჯარაში:">
<!ENTITY showAsDefault.accesskey               "ნ">
<!ENTITY showAsDefaultLast.label               "ბოლოს გამოყენებული საქაღალდე">

<!ENTITY sendOptionsDescription.label          "ტექსტის ფორმატირების წესების მითითება">
<!ENTITY sendOptions.label                     "გაგზავნის პარამეტრები…">
<!ENTITY sendOptions.accesskey                 "პ">

<!ENTITY attachmentReminder.label              "გამოტოვებული დანართების შემოწმება">
<!ENTITY attachmentReminder.accesskey          "ო">
<!ENTITY attachmentReminderOptions.label       "საკვანძო სიტყვები…">
<!ENTITY attachmentReminderOptions.accesskey   "კ">
