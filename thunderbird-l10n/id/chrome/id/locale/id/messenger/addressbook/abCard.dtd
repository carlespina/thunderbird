<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY Contact.tab                     "Kontak">
<!ENTITY Name.box                        "Nama">

<!-- LOCALIZATION NOTE:
 NameField1, NameField2, PhoneticField1, PhoneticField2
 those fields are either LN or FN depends on the target country.
 "FirstName" and "LastName" can be swapped for id to change the order
 but they should not be translated (same applied to phonetic id).
 Make sure the translation of label corresponds to the order of id.
-->

<!-- LOCALIZATION NOTE (NameField1.id) : DONT_TRANSLATE -->
<!-- LOCALIZATION NOTE (NameField2.id) : DONT_TRANSLATE -->
<!-- LOCALIZATION NOTE (PhoneticField1.id) : DONT_TRANSLATE -->
<!-- LOCALIZATION NOTE (PhoneticField2.id) : DONT_TRANSLATE -->

<!ENTITY PhoneticField1.label           "Fonetis:">
<!ENTITY PhoneticField2.label           "Fonetis:">

<!ENTITY PrimaryEmail.label             "Surel:">
<!ENTITY HTML.label                     "HTML">
<!ENTITY Unknown.label                  "Tidak diketahui">
<!ENTITY chatName.label                 "Nama Obrolan:">

<!ENTITY FaxNumber.label                "Faksimile:">

<!ENTITY HomeAddress.label              "Alamat:">
<!ENTITY HomeAddress.accesskey          "a">
<!ENTITY HomeAddress2.label             "">
<!ENTITY HomeAddress2.accesskey         "">
<!ENTITY HomeCity.label                 "Kota:">
<!ENTITY HomeCity.accesskey             "k">
<!ENTITY HomeState.label                "Negara Bagian/Provinsi:">
<!ENTITY HomeState.accesskey            "P">
<!ENTITY HomeZipCode.label              "Kode Pos:">
<!ENTITY HomeZipCode.accesskey          "K">
<!ENTITY HomeCountry.label              "Negara">
<!ENTITY HomeCountry.accesskey          "n">
<!ENTITY HomeWebPage.label              "Laman Web:">
<!ENTITY In.label                       "">
<!ENTITY Year.placeholder               "Tahun">
<!ENTITY Or.value                       "atau">
<!ENTITY Age.placeholder                "Usia">
<!ENTITY YearsOld.label                 "">

<!ENTITY JobTitle.label                 "Judul:">
<!ENTITY Department.label               "Departemen:">
<!ENTITY Company.label                  "Organisasi:">
<!ENTITY WorkAddress.label              "Alamat:">
<!ENTITY WorkAddress2.label             "">
<!ENTITY WorkAddress2.accesskey         "">
<!ENTITY WorkCity.label                 "Kota:">
<!ENTITY WorkCity.accesskey             "k">
<!ENTITY WorkState.label                "Negara Bagian/Provinsi:">
<!ENTITY WorkZipCode.label              "Kode Pos:">
<!ENTITY WorkCountry.label              "Negara:">
<!ENTITY WorkWebPage.label              "Laman Web:">

<!ENTITY Other.tab                      "Lainnya">
<!ENTITY Custom1.accesskey              "1">
<!ENTITY Custom2.accesskey              "2">
<!ENTITY Custom3.accesskey              "3">
<!ENTITY Custom4.accesskey              "4">
<!ENTITY Notes.label                    "Catatan:">

<!ENTITY Chat.tab                       "Obrolan">
<!ENTITY Gtalk.label                    "Google Talk:">
<!ENTITY AIM.label                      "AIM:">
<!ENTITY AIM2.accesskey                 "M">
<!ENTITY Yahoo.label                    "Yahoo!:">
<!ENTITY Yahoo.accesskey                "Y">
<!ENTITY Skype.label                    "Skype:">
<!ENTITY Skype.accesskey                "S">
<!ENTITY QQ.label                       "QQ:">
<!ENTITY QQ.accesskey                   "Q">
<!ENTITY MSN.label                      "MSN:">
<!ENTITY MSN2.accesskey                 "N">
<!ENTITY ICQ.label                      "ICQ:">
<!ENTITY ICQ.accesskey                  "I">

<!ENTITY Photo.tab                      "Foto">
<!ENTITY PhotoFile.label                "Di Komputer ini">
<!ENTITY BrowsePhoto.label              "Jelajahi">
<!ENTITY PhotoURL.label                 "Di Web">
<!ENTITY PhotoDropTarget.label          "Seret foto baru ke sini">

<!ENTITY Contact.accesskey               "C">
<!-- LOCALIZATION NOTE (NameField1.id) : DONT_TRANSLATE -->
<!ENTITY NameField1.id                  "FirstName">
<!-- LOCALIZATION NOTE (NameField2.id) : DONT_TRANSLATE -->
<!ENTITY NameField2.id                  "LastName">
<!-- LOCALIZATION NOTE (PhoneticField1.id) : DONT_TRANSLATE -->
<!ENTITY PhoneticField1.id              "PhoneticFirstName">
<!-- LOCALIZATION NOTE (PhoneticField2.id) : DONT_TRANSLATE -->
<!ENTITY PhoneticField2.id              "PhoneticLastName">
<!ENTITY NameField1.label               "First:">
<!ENTITY NameField1.accesskey           "F">
<!ENTITY NameField2.label               "Last:">
<!ENTITY NameField2.accesskey           "L">
<!ENTITY DisplayName.label              "Display:">
<!ENTITY DisplayName.accesskey          "D">
<!ENTITY preferDisplayName.label        "Always prefer display name over message header">
<!ENTITY preferDisplayName.accesskey    "y">
<!ENTITY NickName.label                 "Nickname:">
<!ENTITY NickName.accesskey             "N">
<!ENTITY PrimaryEmail.accesskey         "E">
<!ENTITY SecondEmail.label              "Additional Email:">
<!ENTITY SecondEmail.accesskey          "i">
<!ENTITY PreferMailFormat.label         "Prefers to receive messages formatted as:">
<!ENTITY PreferMailFormat.accesskey     "v">
<!ENTITY PlainText.label                "Plain Text">
<!ENTITY WorkPhone.label                "Work:">
<!ENTITY WorkPhone.accesskey            "k">
<!ENTITY HomePhone.label                "Home:">
<!ENTITY HomePhone.accesskey            "m">
<!ENTITY FaxNumber.accesskey            "x">
<!ENTITY PagerNumber.label              "Pager:">
<!ENTITY PagerNumber.accesskey          "g">
<!ENTITY CellularNumber.label           "Mobile:">
<!ENTITY CellularNumber.accesskey       "b">
<!ENTITY Home.tab                       "Private">
<!ENTITY Home.accesskey                 "P">
<!ENTITY HomeWebPage.accesskey          "e">
<!ENTITY Birthday.label                 "Birthday:">
<!ENTITY Birthday.accesskey             "B">
<!ENTITY Month.placeholder              "Month">
<!ENTITY Day.placeholder                "Day">
<!ENTITY Age.label                      "Age:">
<!ENTITY Work.tab                       "Work">
<!ENTITY Work.accesskey                 "W">
<!ENTITY JobTitle.accesskey             "i">
<!ENTITY Department.accesskey           "m">
<!ENTITY Company.accesskey              "n">
<!ENTITY WorkAddress.accesskey          "d">
<!ENTITY WorkState.accesskey            "S">
<!ENTITY WorkZipCode.accesskey          "Z">
<!ENTITY WorkCountry.accesskey          "u">
<!ENTITY WorkWebPage.accesskey          "e">
<!ENTITY Other.accesskey                "h">
<!ENTITY Custom1.label                  "Custom 1:">
<!ENTITY Custom2.label                  "Custom 2:">
<!ENTITY Custom3.label                  "Custom 3:">
<!ENTITY Custom4.label                  "Custom 4:">
<!ENTITY Notes.accesskey                "N">
<!ENTITY Chat.accesskey                 "a">
<!ENTITY Gtalk.accesskey                "G">
<!ENTITY XMPP.label                     "Jabber ID:">
<!ENTITY XMPP.accesskey                 "J">
<!ENTITY IRC.label                      "IRC Nick:">
<!ENTITY IRC.accesskey                  "R">
<!ENTITY Photo.accesskey                "o">
<!ENTITY GenericPhoto.label             "Generic Photo">
<!ENTITY GenericPhoto.accesskey         "G">
<!ENTITY DefaultPhoto.label             "Default">
<!ENTITY PhotoFile.accesskey            "n">
<!ENTITY BrowsePhoto.accesskey          "r">
<!ENTITY PhotoURL.accesskey             "b">
<!ENTITY PhotoURL.placeholder           "Paste or type the web address of a photo">
<!ENTITY UpdatePhoto.label              "Update">
<!ENTITY UpdatePhoto.accesskey          "u">
