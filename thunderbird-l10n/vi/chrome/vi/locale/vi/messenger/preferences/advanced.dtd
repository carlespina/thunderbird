<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Tổng quát">
<!ENTITY dataChoicesTab.label    "Lựa chọn dữ liệu">
<!ENTITY itemUpdate.label        "Cập nhật">
<!ENTITY itemNetworking.label    "Mạng &amp; dung lượng trống">
<!ENTITY itemCertificates.label  "Chứng chỉ">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.accesskey   "G">
<!ENTITY dateTimeFormatting.label      "Định dạng ngày và giờ">
<!ENTITY languageSelector.label        "Ngôn ngữ">
<!ENTITY allowHWAccel.label            "Sử dụng chế độ tăng tốc phần cứng khi khả dụng">
<!ENTITY allowHWAccel.accesskey        "h">
<!ENTITY storeType.label               "Kiểu lưu trữ thư cho tài khoản mới:">
<!ENTITY storeType.accesskey           "T">
<!ENTITY mboxStore2.label              "Tập tin trên mỗi thư mục (mbox)">
<!ENTITY maildirStore.label            "Tập tin trên mỗi thư (maildir)">

<!ENTITY scrolling.label               "Cuộn">
<!ENTITY useAutoScroll.label           "Sử dụng tự động cuộn">
<!ENTITY useAutoScroll.accesskey       "U">
<!ENTITY useSmoothScrolling.label      "Cuộn uyển chuyển">
<!ENTITY useSmoothScrolling.accesskey  "m">

<!ENTITY systemIntegration.label       "Hệ thống tích hợp">
<!ENTITY alwaysCheckDefault.label      "Luôn kiểm tra xem &brandShortName; có phải là ứng dụng thư mặc định khi khởi động">
<!ENTITY alwaysCheckDefault.accesskey  "A">
<!ENTITY searchIntegration.label       "Cho phép &searchIntegration.engineName; tìm kiếm thư">
<!ENTITY searchIntegration.accesskey   "S">
<!ENTITY checkDefaultsNow.label        "Kiểm tra ngay…">
<!ENTITY checkDefaultsNow.accesskey    "N">
<!ENTITY configEditDesc.label          "Cấu hình nâng cao">
<!ENTITY configEdit.label              "Trình chỉnh sửa cấu hình…">
<!ENTITY configEdit.accesskey          "C">
<!ENTITY showReturnReceipts.accesskey  "R">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Telemetry">
<!ENTITY telemetryDesc.label             "Chia sẻ hiệu suất, cách sử dụng, phần cứng và dữ liệu tùy chỉnh về ứng dụng email khách của bạn với &vendorShortName; để giúp chúng tôi làm cho &brandShortName; tốt hơn">
<!ENTITY enableTelemetry.label           "Bật Telemetry">
<!ENTITY enableTelemetry.accesskey       "T">
<!ENTITY telemetryLearnMore.label        "Tìm hiểu thêm">

<!ENTITY crashReporterSection.label      "Trình báo cáo lỗi">
<!ENTITY crashReporterDesc.label         "&brandShortName; gửi báo cáo sự cố để giúp &vendorShortName; làm cho ứng dụng email của bạn ổn định và an toàn hơn">
<!ENTITY enableCrashReporter.label       "Kích hoạt trình báo cáo lỗi">
<!ENTITY enableCrashReporter.accesskey   "C">
<!ENTITY crashReporterLearnMore.label    "Tìm hiểu thêm">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "Cập nhật &brandShortName;">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Phiên bản ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAppAllow.description      "Cho phép &brandShortName;">
<!ENTITY updateAuto.label                "Tự động cài đặt các cập nhật (nên làm vì tăng tính bảo mật)">
<!ENTITY updateAuto.accesskey            "A">
<!ENTITY updateCheck.label               "Kiểm tra cập nhật, nhưng hãy để tôi chọn có cài đặt chúng không">
<!ENTITY updateCheck.accesskey           "C">
<!ENTITY updateHistory.label             "Hiển thị lịch sử cập nhật">
<!ENTITY updateHistory.accesskey         "p">

<!ENTITY useService.label                "Sử dụng dịch vụ chạy nền để cài đặt các cập nhật">
<!ENTITY useService.accesskey            "b">

<!ENTITY updateCrossUserSettingWarning.description "Cài đặt này sẽ áp dụng cho tất cả các tài khoản Windows và các cấu hình của &brandShortName; sử dụng cài đặt này của &brandShortName;.">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Cài đặt…">
<!ENTITY showSettings.accesskey        "S">
<!ENTITY proxiesConfigure.label        "Định cấu hình cách &brandShortName; kết nối Internet">
<!ENTITY connectionsInfo.caption       "Kết nối">
<!ENTITY offlineInfo.caption           "Ngoại tuyến">
<!ENTITY offlineInfo.label             "Cấu hình thiết lập ngoại tuyến">
<!ENTITY showOffline.label             "Ngoại tuyến…">
<!ENTITY showOffline.accesskey         "O">

<!ENTITY Diskspace                       "Dung lượng đĩa">
<!ENTITY offlineCompactFolders.label     "Nén tất cả các thư mục khi nó sẽ lưu lại">
<!ENTITY offlineCompactFolders.accesskey "a">
<!ENTITY offlineCompactFoldersMB.label   "MB tổng cộng">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Sử dụng tối đa">
<!ENTITY useCacheBefore.accesskey        "U">
<!ENTITY useCacheAfter.label             "MB dung lượng đĩa cho bộ đệm">
<!ENTITY overrideSmartCacheSize.label    "Ghi đè quản lý bộ đệm tự động">
<!ENTITY overrideSmartCacheSize.accesskey "v">
<!ENTITY clearCacheNow.label             "Xóa ngay">
<!ENTITY clearCacheNow.accesskey         "C">

<!-- Certificates -->
<!ENTITY certSelection.description       "Khi một máy chủ yêu cầu chứng chỉ cá nhân của tôi:">
<!ENTITY certs.auto                      "Tự động chọn một cái">
<!ENTITY certs.auto.accesskey            "S">
<!ENTITY certs.ask                       "Hỏi tôi mọi lúc">
<!ENTITY certs.ask.accesskey             "A">
<!ENTITY enableOCSP.label                "Truy vấn máy chủ đáp ứng giao thức OCSP để xác minh hiệu lực của các chứng chỉ">
<!ENTITY enableOCSP.accesskey            "Q">

<!ENTITY manageCertificates.label "Quản lý chứng chỉ">
<!ENTITY manageCertificates.accesskey "M">
<!ENTITY viewSecurityDevices.label "Thiết bị bảo mật">
<!ENTITY viewSecurityDevices.accesskey "D">

<!ENTITY manageCertificates2.label "Quản lý chứng chỉ…">
<!ENTITY manageCertificates2.accesskey "M">
<!ENTITY viewSecurityDevices2.label "Thiết bị bảo mật…">
<!ENTITY viewSecurityDevices2.accesskey "D">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Enable Global Search and Indexer">
<!ENTITY returnReceiptsInfo.label      "Determine how &brandShortName; handles return receipts">
<!ENTITY showReturnReceipts.label      "Return Receipts…">
