<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  fontsAndEncodingsDialog.title           "Phông chữ &amp; bảng mã">

<!ENTITY  language.label                          "Phông chữ cho:">
<!ENTITY  language.accesskey                      "t">

<!ENTITY  size.label                              "Kích cỡ:">
<!ENTITY  sizeProportional.accesskey              "e">
<!ENTITY  sizeMonospace.accesskey                 "i">

<!ENTITY  proportional.label                      "Tỷ lệ:">
<!ENTITY  proportional.accesskey                  "P">

<!ENTITY  serif.label                             "Serif:">
<!ENTITY  serif.accesskey                         "S">
<!ENTITY  sans-serif.label                        "Sans-serif:">
<!ENTITY  sans-serif.accesskey                    "n">
<!ENTITY  monospace.label                         "Monospace:">
<!ENTITY  monospace.accesskey                     "M">

<!-- LOCALIZATION NOTE (font.langGroup.latin) :
     Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language. -->
<!ENTITY  font.langGroup.latin                    "Latin">
<!ENTITY  font.langGroup.japanese                 "Nhật Bản">
<!ENTITY  font.langGroup.trad-chinese             "Hoa Phồn Thể (Đài Loan)">
<!ENTITY  font.langGroup.simpl-chinese            "Hoa Giản Thể">
<!ENTITY  font.langGroup.trad-chinese-hk          "Hoa Phồn Thể (Hồng Kông)">
<!ENTITY  font.langGroup.korean                   "Hàn Quốc">
<!ENTITY  font.langGroup.cyrillic                 "Kirin">
<!ENTITY  font.langGroup.el                       "Hi Lạp">
<!ENTITY  font.langGroup.other                    "Hệ thống chữ viết khác">
<!ENTITY  font.langGroup.thai                     "Thái">
<!ENTITY  font.langGroup.hebrew                   "Do Thái">
<!ENTITY  font.langGroup.arabic                   "Ả Rập">
<!ENTITY  font.langGroup.devanagari               "Devanagari">
<!ENTITY  font.langGroup.tamil                    "Tamil">
<!ENTITY  font.langGroup.armenian                 "Armenia">
<!ENTITY  font.langGroup.bengali                  "Băng-gan">
<!ENTITY  font.langGroup.canadian                 "Kí hiệu Âm tiết Canađa Thống nhất">
<!ENTITY  font.langGroup.ethiopic                 "Ethiopic">
<!ENTITY  font.langGroup.georgian                 "Gruzia">
<!ENTITY  font.langGroup.gujarati                 "Gujarat">
<!ENTITY  font.langGroup.gurmukhi                 "Gurmukhi">
<!ENTITY  font.langGroup.khmer                    "Khơ-me">
<!ENTITY  font.langGroup.malayalam                "Malayalam">
<!ENTITY  font.langGroup.math                     "Mathematics">
<!ENTITY  font.langGroup.odia                     "Tiếng Odia">
<!ENTITY  font.langGroup.telugu                   "Tiếng Telugu">
<!ENTITY  font.langGroup.kannada                  "Tiếng Kannada">
<!ENTITY  font.langGroup.sinhala                  "Tiếng Sinhala">
<!ENTITY  font.langGroup.tibetan                  "Tiếng Tibetan">
<!-- Minimum font size -->
<!ENTITY minSize.label                            "Cỡ phông tối thiểu:">
<!ENTITY minSize.accesskey                        "z">
<!ENTITY minSize.none                             "Không">

<!-- default font type -->
<!ENTITY  useDefaultFontSerif.label               "Serif">
<!ENTITY  useDefaultFontSansSerif.label           "Sans Serif">

<!-- fonts in message -->
<!ENTITY  fontControl.label                       "Điều chỉnh phông chữ">
<!ENTITY  useFixedWidthForPlainText.label         "Sử dụng phông chữ có chiều rộng cố định cho thư văn bản thuần túy">
<!ENTITY  fixedWidth.accesskey                    "x">
<!ENTITY  useDocumentFonts.label                  "Cho phép thư sử dụng phông chữ khác">
<!ENTITY  useDocumentFonts.accesskey              "o">

<!-- Language settings -->
<!ENTITY sendDefaultCharset.label         "Thư gửi đi:">
<!ENTITY sendDefaultCharset.accesskey     "u">
<!ENTITY languagesTitle2.label            "Bảng mã văn bản">
<!ENTITY composingDescription2.label      "Đặt mã hóa văn bản mặc định để gửi và nhận thư">

<!ENTITY viewDefaultCharsetList.label     "Thư đến:">
<!ENTITY viewDefaultCharsetList.accesskey  "I">
<!ENTITY replyInDefaultCharset3.label     "Khi có thể, sử dụng mã hóa văn bản mặc định trong trả lời">
<!ENTITY replyInDefaultCharset3.accesskey "h">
