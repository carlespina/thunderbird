# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

saved-logins =
    .title = Spremljene prijave
window-close =
    .key = w
focus-search-shortcut =
    .key = f
focus-search-altshortcut =
    .key = k
copy-provider-url-cmd =
    .label = Kopiraj URL
    .accesskey = j
copy-username-cmd =
    .label = Kopiraj korisničko ime
    .accesskey = o
edit-username-cmd =
    .label = Uredi korisničko ime
    .accesskey = d
copy-password-cmd =
    .label = Kopiraj lozinku
    .accesskey = l
edit-password-cmd =
    .label = Uredi lozinku
    .accesskey = e
search-filter =
    .accesskey = T
    .placeholder = Traži
column-heading-provider =
    .label = Davatelj usluge
column-heading-username =
    .label = Korisničko ime
column-heading-password =
    .label = Lozinka
column-heading-time-created =
    .label = Prvi put korišteno
column-heading-time-last-used =
    .label = Posljednji put korišteno
column-heading-time-password-changed =
    .label = Zadnja promjena
column-heading-times-used =
    .label = Broj korištenja
remove =
    .label = Ukloni
    .accesskey = U
import =
    .label = Uvoz…
    .accesskey = v
close-button =
    .label = Zatvori
    .accesskey = Z
show-passwords =
    .label = Prikaži lozinke
    .accesskey = P
hide-passwords =
    .label = Sakrij lozinke
    .accesskey = S
logins-description-all = Prijave za sljedeće davatelje usluge su spremljene na vašem računalu
logins-description-filtered = Sljedeće prijave odgovaraju vašoj pretrazi:
remove-all =
    .label = Ukloni sve
    .accesskey = s
remove-all-shown =
    .label = Ukloni sve prikazano
    .accesskey = k
remove-all-passwords-prompt = Jeste li sigurni da želite ukloniti sve lozinke?
remove-all-passwords-title = Ukloni sve lozinke
no-master-password-prompt = Jeste li sigurni da želite prikazati svoje lozinke?
