# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Issin ugar
accessibility-text-label-header = Tibzimin d yismawen n uḍris

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Alɣu
accessibility-fail =
    .alt = Tuccḍa
accessibility-best-practices =
    .alt = Amahil ifazen

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Seqdec <code>alt</code> i tmerna n tibzimin i yiferdisen <div>area</div> yesɛan imyerr <span>href</span>. <a>Issin ugar</a>
accessibility-text-label-issue-dialog = Adiwen yessefk ad yesɛu tabzimt. <a>Issin ugar</a>
accessibility-text-label-issue-document-title = ISemliyen yessefk ad sɛun aferdis <code>title</code>.<a>Issin ugar</a>
accessibility-text-label-issue-embed = Agbur usliɣ yessefk ad yesɛu tabzimt. <a>Issin ugar</a>
